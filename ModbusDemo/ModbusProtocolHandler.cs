﻿using Modbus.Device;
using System;

namespace ModbusDemo
{
    class ModbusProtocolHandler
    {
        private IModbusSerialMaster _master;

        public ModbusProtocolHandler(IModbusSerialMaster master)
        {
            _master = master;
        }

        public ushort[] readRegister(byte deviceAddr, ushort startAddr, ushort count)
        {
            byte slaveId = deviceAddr;

            var result = _master.ReadHoldingRegisters(slaveId, startAddr, count);

            return result;
        }
        public ushort[] readRegister(IModbusSerialMaster master, byte deviceAddr, ushort startAddr, ushort count)
        {
            byte slaveId = deviceAddr;

            var result = master.ReadHoldingRegisters(slaveId, startAddr, count);

            return result;
        }

        public void writeRegisters(int deviceAddr, ushort[] addresses,
            ushort[] values)
        {
            byte slaveId = (byte)deviceAddr;

            for (int i = 0; i < addresses.Length; i++)
            {
                try
                {
                    _master.WriteSingleRegister(slaveId, addresses[i], values[i]);
                }
                catch (System.IO.IOException e)
                {
                    Console.WriteLine(e.ToString());
                }
                catch (System.TimeoutException)
                {
                    throw new TimeoutException();
                }
            }
        }
    }
}
