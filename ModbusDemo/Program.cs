﻿using Modbus.Device;
using System;
using System.IO.Ports;

namespace ModbusDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] ports = SerialPort.GetPortNames();

            if (ports.Length == 0)
            {
                Console.WriteLine("No serial (COM) port available.");
                return;
            }

            var portName = ports[0];
            var portSpeed = 9600;
            var portParity = Parity.None;
            var bitData = (byte)8;
            var stopBits = StopBits.One;

            var serialPort = new SerialPort(portName,
                portSpeed,
                portParity,
                bitData,
                stopBits);

            serialPort.ReadTimeout = 200;
            serialPort.WriteTimeout = 200;

            serialPort.Open();

            var master = ModbusSerialMaster.CreateRtu(serialPort);

            ModbusProtocolHandler modbusProtocolHandler = new ModbusProtocolHandler(master);

            byte deviceAddr = 0;
            ushort startAddr = 0;
            ushort count = 1;

            //Read something
            var readResult = modbusProtocolHandler.readRegister(deviceAddr, startAddr, count);

            Console.WriteLine("Read result is " + readResult);

            serialPort.Close();
        }
    }
}
